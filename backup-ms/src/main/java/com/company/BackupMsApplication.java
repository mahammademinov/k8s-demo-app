package com.company;

import ch.sbb.esta.openshift.gracefullshutdown.GracefulshutdownSpringApplication;
import com.company.service.BackupService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
public class BackupMsApplication implements CommandLineRunner {

    private final BackupService backupService;

    public static void main(String[] args) {
        GracefulshutdownSpringApplication.run(BackupMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        backupService.backUp();
    }
}
