package com.company.client;


import com.company.entity.Employee;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "employee", url = "${ms.employees.url}")
public interface EmployeeFeignClient {

     @GetMapping
     ResponseEntity<List<Employee>> findAllEmployees();
}
