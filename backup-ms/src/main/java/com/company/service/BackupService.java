package com.company.service;

import com.company.client.EmployeeFeignClient;
import com.company.entity.Employee;
import com.company.repository.EmployeeRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BackupService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeFeignClient employeeFeignClient;

    public void backUp() {
        List<Employee> employees = employeeFeignClient.findAllEmployees().getBody();
        if (employees != null && !employees.isEmpty()) {
            employeeRepository.saveAll(employees);
        }
    }
}
