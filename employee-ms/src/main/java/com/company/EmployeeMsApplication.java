package com.company;

import com.company.entity.Employee;
import com.company.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class EmployeeMsApplication implements CommandLineRunner {

    private final EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(EmployeeMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Employee employee1 = Employee.builder()
                .id(1L)
                .name("Test1")
                .email("test1@mail.ru")
                .department("TestDepartment1")
                .build();

        Employee employee2 = Employee.builder()
                .id(2L)
                .name("Test2")
                .email("test2@mail.ru")
                .department("TestDepartment2")
                .build();

        employeeRepository.save(employee1);
        employeeRepository.save(employee2);
    }
}
