package com.company.controller;

import com.company.entity.Employee;
import com.company.service.EmployeeService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/employees")
@RequiredArgsConstructor
@Slf4j
public class EmployeeController {

    private final EmployeeService service;

    @ResponseBody
    @GetMapping("/hello")
    public String hello(){
        return service.hello();
    }

    @GetMapping
    public ResponseEntity<List<Employee>> findAllEmployees(){
        return ResponseEntity.ok(service.findAllEmployees());
    }

    @GetMapping("/list")
    public String getAllEmployees(Model model) {
        return service.getAllEmployees(model);
    }

    @GetMapping("/addEmployeeForm")
    public String addEmployeeForm(Model model) {
        log.info("add employee api called");
        return service.addEmployeeForm(model);
    }

    @PostMapping("/saveEmployee")
    public String saveEmployee(@ModelAttribute Employee employee) {
        return service.saveEmployee(employee);
    }
}