package com.company.service;

import com.company.entity.Employee;
import com.company.repository.EmployeeRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public String hello(){
        return "index2";
    }

    public List<Employee> findAllEmployees(){
        return employeeRepository.findAll();
    }

    public String getAllEmployees(Model model) {
        model.addAttribute("employees", employeeRepository.findAll());
        return "list-employees";
    }

    public String addEmployeeForm(Model model) {
        Employee newEmployee = new Employee();
        model.addAttribute("employee", newEmployee);
        return "index";
    }

    public String saveEmployee(@ModelAttribute Employee employee) {
        employeeRepository.save(employee);
        return "redirect:/list";
    }
}
